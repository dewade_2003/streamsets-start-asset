# ClassLoaderStageLibraryTask

## 实例化 

该类实际上是通过Dagger通过@Inject注解进行的实例化，并且定义了该类在Dagger中的名称为`stageLibrary`

```java
package com.streamsets.datacollector.stagelibrary;
...
public class ClassLoaderStageLibraryTask extends AbstractTask implements StageLibraryTask {
  @Inject // 这里标记给Dagger进行构造
  public ClassLoaderStageLibraryTask(RuntimeInfo runtimeInfo, BuildInfo buildInfo, Configuration configuration) {
    super("stageLibrary");  // task 名称
    this.runtimeInfo = runtimeInfo;
    this.buildInfo = buildInfo;
    this.configuration = configuration;
    Map<String, String> aliases = new HashMap<>();
    for (Map.Entry<String,String> entry
        : configuration.getSubSetConfiguration(CONFIG_LIBRARY_ALIAS_PREFIX).getValues().entrySet()) {
      aliases.put(entry.getKey().substring(CONFIG_LIBRARY_ALIAS_PREFIX.length()), entry.getValue());
    }
    libraryNameAliases = ImmutableMap.copyOf(aliases);
    aliases.clear();
    for (Map.Entry<String,String> entry
      : configuration.getSubSetConfiguration(CONFIG_STAGE_ALIAS_PREFIX).getValues().entrySet()) {
      aliases.put(entry.getKey().substring(CONFIG_STAGE_ALIAS_PREFIX.length()), entry.getValue());
    }
    stageNameAliases = ImmutableMap.copyOf(aliases);
  }
}
```

## 相关引用范围

#### 1. DataCollectorMain中的doMain()方法，用来实现对library的加载。

这个类其实就是`com.streamsets.datacollector.main.DataCollectorMain`类中doMain()方法中调用的`AbstractTask`的实例。

```java
package com.streamsets.datacollector.main;
...
public class Main {
...
public Main(ObjectGraph dagger, Task task, Callable<Boolean> taskStopCondition) {
    this.dagger = dagger;
    if (task == null) {
      task = dagger.get(TaskWrapper.class);
    }
    this.task = task;
    this.taskStopCondition = taskStopCondition;
  }
}
```



![image-20200407133850854](./image/image-20200407133850854.png)



#### 2. PreviewResource对外提供REST接口

关于该接口的实现过程可以参考`3.StreamSets-抽象结构梳理.vsdx`文档中`PrviewResource`的梳理过程。

`PrviewResource`通过`AsyncPreviewer`异步回调了`SyncPreviewer`的`start`函数，调用了`buildPreviewPipeline`函数构造了`PreviewPipelineBuilder`实例。该实例的`StageLibraryTask`成员变量，应该也是本文主题`ClassLoaderStageLibraryTask`的实例，其也是被Dagger通过接口`StageLibraryTask`注入的。

接口的服务层面主要调用了`EventHandlerTask`的实现类`RemoteEventHandlerTask`相关函数功能。

![image-20200407184818656](./image/image-20200407184818656.png)



## 后续

梳理到这里，发现StreamSets的核心模块container中：

1. 核心抽象模版实现在container-common模块中。
2. 众多异步回调的方式，很多包中使用了Callable、Runnable、Future的实现。
3. 不少包中都使用了dagger子包，创建了以XxxModule作为后缀的类，用于对包内类的注入进行了配置。例如`execution`
4. `config`包中定义XxxDefinition作为类加载器的输入模版元。
5. `definition`包中定义了XxxDefinitionExtractor的类，实现了作为默认的模板元提取方法`extract`。
6. 其中一部分任务管理在stagesupport模块之中。
7. 前端界面相关的交互消息体在messageing-client模块之中。









